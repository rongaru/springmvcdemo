package com.bway.springmvcdemo.dao;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bway.springmvcdemo.model.User;


@Repository
public class UserDaoImpl implements UserDao{
	
	@Resource
	private SessionFactory sessionFactory; //hibernate bean object

	@Override
	@Transactional
	public void signup(User user) {
		
		Session sess = sessionFactory.getCurrentSession();
		sess.save(user);
	}

	@Override
	@Transactional
	public User login(String un, String pwd) {
		// TODO Auto-generated method stub
		Session sess = sessionFactory.getCurrentSession();
		Criteria crt = sess.createCriteria(User.class);
		
		crt.add(Restrictions.eq("username",un))
		.add(Restrictions.eq("password", pwd));
		
		return (User)crt.uniqueResult();
	}

}
