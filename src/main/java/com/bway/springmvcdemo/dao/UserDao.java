package com.bway.springmvcdemo.dao;

import com.bway.springmvcdemo.model.User;

public interface UserDao {
	
	void signup(User user);
	User login(String un, String pwd);

}
