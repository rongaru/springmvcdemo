package com.bway.springmvcdemo.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bway.smvcdemo.utilities.VerifyRecaptcha;
import com.bway.springmvcdemo.HomeController;
import com.bway.springmvcdemo.dao.EmployeeDao;
import com.bway.springmvcdemo.dao.EmployeeDaoImpl;
import com.bway.springmvcdemo.dao.UserDao;
import com.bway.springmvcdemo.model.User;

@Controller
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private UserDao udao;
	
	@Autowired
	private EmployeeDao edao;
	
	
	@RequestMapping (value="/userLogin", method = RequestMethod.GET)
	public String getLoginForm() {
		
		logger.info("open login form");
		return "login";
	}
	
	@RequestMapping(value="/userLogin", method = RequestMethod.POST)
	public String userLogin(@ModelAttribute User u, Model model,HttpSession httpSession,HttpServletRequest request) throws IOException {
		
		String input = request.getParameter("g-recaptcha-response");
		boolean result = VerifyRecaptcha.verify(input);

		if (result) {

			u.setPassword(DigestUtils.md5DigestAsHex(u.getPassword().getBytes()));
			User user = udao.login(u.getUsername(), u.getPassword());

			if (user != null) {

				logger.info("login sucesss");
				model.addAttribute("un", u.getUsername());
				model.addAttribute("elist", edao.getAllEmployee());
				httpSession.setAttribute("active", user);
				httpSession.setMaxInactiveInterval(180);

				return "home";
			} else {
				logger.info("login fail");
				model.addAttribute("error", "user does not exits");
				return "login";
			}
			
		}
			
			logger.info("login fail");
			model.addAttribute("error", "you are not human");
			return "login";
	}
	
	@ RequestMapping(value = "/logout")
	public String logout(HttpSession httpSession) {
		
		httpSession.invalidate();
		return "login";
	}
	
	
	
	@RequestMapping(value = "/facebook", method = RequestMethod.GET)
	public String facebookLogin(Model model) {
		
		model.addAttribute("elist", edao.getAllEmployee());
		
		return "redirect:https://www.facebook.com/dialog/oauth?client_id=817167695373956&redirect_uri=http://localhost:8080/springmvcdemo/authorize/facebook&response_type=code&scope=email";
	}
}
