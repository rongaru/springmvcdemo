package com.bway.springmvcdemo.controller;

import java.io.File;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GalleryController {
	
	@RequestMapping(value = "/gallery")
	public String gallery(Model model) {
		
		File dir = new File("/home/roshan/Documents/workspace-sts-3.9.11.RELEASE/springmvcdemo/src/main/webapp/resources/imgs");
		String[] imglist= dir.list();
		
		model.addAttribute("img", imglist);
	
		return "gallery";
	}
	
}
