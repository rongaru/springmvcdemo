package com.bway.springmvcdemo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bway.springmvcdemo.dao.UserDao;
import com.bway.springmvcdemo.model.User;

@Controller
public class SignUpController {
	
	@Autowired // udao = new UserDaoImpl()
	private UserDao udao;
	
	@RequestMapping(value="/userSignUp", method = RequestMethod.GET)
	public String getSignUpForm() {
		return "signUp";
		
	}
	
	@RequestMapping(value="/userSignUp", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute User user,Model model) {
		
		user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
		udao.signup(user);
		model.addAttribute("msg","user registered");
		return "login";
	}

}
