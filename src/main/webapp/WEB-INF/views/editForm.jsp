<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%@ include file="header.jsp" %>
	<h1>Employee Form</h1>
	<hr>
	<spring:form action="${pageContext.request.contextPath}/update" method="post" modelAttribute="emodel">
		<table>

			<tr>
				<td>FirstName</td>
				<td><spring:input path="fname" /></td>
			</tr>

			<tr>
				<td>LastName</td>
				<td><spring:input path="lname" /></td>
			</tr>

			<tr>
				<td>Gender</td>
				<td><spring:radiobutton path="gender" value="male" /></td>
				<td><spring:radiobutton path="gender" value="female" /></td>
			</tr>

			<tr>
				<td>DoB</td>
				<td><spring:input path="dob" type="date" /></td>
			</tr>

			<tr>
				<td>JoingDate</td>
				<td><spring:input path="joiningDate" type="date" /></td>
			</tr>

			<tr>
				<td>Company</td>
				<td><spring:input path="company" /></td>
			</tr>

			<tr>
				<td>Post</td>
				<td><spring:input path="post" /></td>
			</tr>

			<tr>
				<td>Phone</td>
				<td><spring:input path="phone" /></td>
			</tr>

			<tr>
				<td>Email</td>
				<td><spring:input path="email" /></td>
			</tr>

			<tr>
				<td>Country</td>
				<td><spring:input path="address.country" /></td>
			</tr>

			<tr>
				<td>State</td>
				<td><spring:select path="address.state">
						<spring:option value="">------select-------</spring:option>
						<spring:option value="state-1">State-1</spring:option>
						<spring:option value="state-2">State-2</spring:option>
						<spring:option value="state-3">State-3</spring:option>
						<spring:option value="state-4">State-4</spring:option>
						<spring:option value="state-5">State-5</spring:option>
						<spring:option value="state-6">State-6</spring:option>
						<spring:option value="state-7">State-7</spring:option>
					</spring:select></td>
			</tr>

			<tr>
				<td>zip</td>
				<td><spring:input path="address.zip" /></td>
			</tr>

			<tr>
				<td>FirstName</td>
				<td><input type="submit" value="update"></td>
			</tr>
		</table>
		
		<spring:hidden path="id"/>
		<spring:hidden path="address.id"/>
	
	</spring:form>
</body>
</html>