<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	<title>Home</title>
</head>
<body>

	<%@ include file = "header.jsp" %>

<table id="myTable" class="table table-stripted">
	<thead>
		<tr class="success">
			<td>FirstName</td>
			<td>LastName</td>
			<td>Company</td>
			<td>Phone</td>
			<td>City</td>
			<td>Action</td>
		</tr>
	</thead>
	<tbody>
			<c:forEach var="emp" items="${elist}">
				<tr class="danger">
					<td>${emp.fname}</td>
					<td>${emp.lname}</td>
					<td>${emp.company}</td>
					<td>${emp.phone}</td>
					<td>${emp.address.city}</td>
					<td>
						<input type="submit" value="Edit" onclick="editEmp(${emp.id})" class="btn btn-success">
						<input type="submit" value="delete" onclick="deleteEmp(${emp.id})" class="btn btn-danger">
					</td>
				</tr>
			</c:forEach>
		</tbody>
</table>


<script type="text/javascript">
function editEmp(id){
	window.location="${pageContext.request.contextPath}/"+id+"/edit";
}

function deleteEmp(id){
	var msg = confirm("do you want to delete?");
	if(msg==true){
	window.location="${pageContext.request.contextPath}/"+id+"/delete";
	}
}

$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
</body>
</html>
