<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file = "header.jsp" %>
	<c:forEach var="imgname" items="${img }">
		<a href="resources/imgs/${imgname}" target="_blank">
		<img src="${pageContext.request.contextPath}/resources/imgs/${imgname}" width="300px" height="300px">
		</a>
	</c:forEach>

</body>
</html>